@extends('layouts.master')
@section('title')
    <title>Login - {{ env('APP_NAME') }}</title>
@endsection
@section('content')
<main class="login-header">
    <div class="login-body text-dark col-12">
        <div class="row">
            <div class="col-3">
            <a href="" class="">
                <img src="{{ asset('icon/logo-sekolah/14.png') }}" alt="" class="icon-back" />
            </a>
            </div>

            <div class="col-9 col-md-7">
            <h6 class="login-title text-left pt-4 text-error">
                SMK MUHAMMADIYAH 1 WELERI
            </h6>
            </div>

            <div class="col-12 mt-5">
            <h3 class="h-3">
                <strong>Login</strong>
            </h3>
            <p class="text-muted" style="font-size: 14px;">Masukkan nomor telepon Anda</p>
            </div>

            <div class="col-12 mt-5">
            <h6 style="font-size: 12px;"><strong>No Telepon</strong></h6>
            <input id="frist_name" type="" class="form-control" name="frist_name" placeholder="0000 0000 0000"
                autofocus="" data-height="55" style="border: 1px solid #CACCCF;
                    box-sizing: border-box;
                    border-radius: 10px;" />
            </div>
        </div>
        <div class="mt-5">
            <a href="javascript:;" class="btn btn-negative follow-btn btn-block" style="border: 1px solid #FF9463;
                box-sizing: border-box;
                box-shadow: 3px 4px 10px rgba(255, 148, 99, 0.25);
                border-radius: 25px; padding-top: 10px">Lanjut</a>
            <p class="text-muted text-center mt-3 mb-3" style="font-size: 14px;"> Atau daftar Dengan</p>
        </div>
    </div>

    <div class="col-12 mt-5">
        <p class="text-center" style="font-size: 14px;">Belum Punya Akun? <a href="#" class="text-danger"> Daftar </a>
        </p>
    </div>
</main>
@endsection
