<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  @yield('title')
  <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('assets/images/favicon/apple-touch-icon.png') }}" />
  <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('assets/images/favicon/favicon-32x32.png') }}" />
  <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets/images/favicon/favicon-16x16.png') }}" />
  <link rel="manifest" href="manifest.webmanifest" />

  <meta name="mobile-web-app-capable" content="yes" />
  <meta name="apple-mobile-web-app-capable" content="yes" />
  <meta name="msapplication-starturl" content="/" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <meta name="theme-color" content="#30aee4" />

  <!-- General CSS -->
  <link rel="stylesheet" href="{{ asset('assets/dist/css/bootstrap.min.css') }}" />

  <!-- CSS Libraries -->
  <link rel="stylesheet" href="{{ asset('assets/css/pages/authentication.css') }}" />

  <!-- Template CSS -->
  <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}" />
</head>

<body>
    <div id="login" class="app app-navbar container">
        
        @yield('content')

        <div class="loading-page" style="display:none">
        <div class="loading-dialog">
            <div class="loading-content">
            <div class="lds-spinner">
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
            </div>
            </div>
        </div>
        </div>
    </div>

  <!-- General JS Scripts -->
  <script src="{{ asset('assets/js/jquery-3.5.1.min.js') }}"></script>
  <script src="{{ asset('assets/dist/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('assets/js/service-worker.js') }}"></script>
</body>

</html>