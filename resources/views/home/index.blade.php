@extends('layouts.master_bottom_navigation')
@section('title')
<title>Home - {{ env('APP_NAME') }}</title>
@endsection
@section('style')
    <style>
        .banner-home {
            height: 150px;
            width: 100%;
            border-radius: 10px;
            background-image: url('./../icon/banner/banner-home.svg');
            background-repeat: no-repeat;
            background-size: cover;
            padding-right: 30%;
            box-shadow: 1px 2px 4px #cccccc;
        }

        .fitur {
            height: 50px;
            width: 50px;
            /* background-color: white; */
            /* background-image: url('./../icon/fitur/list-icon.svg'); */
            background-position: center;
            background-repeat: no-repeat;
            border-radius: 50%;
            box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.06);
        }

        .tugas-home {
            height: 30px;
            width: 30px;
            /* background-color: white; */
            /* background-image: url('./../icon/fitur/list-icon.svg'); */
            background-position: center;
            background-repeat: no-repeat;
            border-radius: 50%;
            box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.06);
        }

        .fitur-frame {
            background-color: white;
        }

        .tugas-home-frame {
            background-color: white;
        }

        .judul-section {
            font-size: 14px;
            font-family: 'Open Sans';
            letter-spacing: -0.0555556px;
            line-height: 19px;
            font-style: normal;
            font-weight: 600;
        }

        .pengumuman-list-x {
            -webkit-overflow-scrolling: touch;
            overflow-x: scroll;
            overflow-y: hidden;
            white-space: nowrap;
        }

        .pengumuman-card {
            height: 125px;
            width: 100%;
            border-radius: 10px;
            display: inline-block;
            background-repeat: no-repeat;
            background-size: cover;
            box-shadow: 1px 2px 4px #cccccc;
        }

        .tugas-list-x {
            -webkit-overflow-scrolling: touch;
            overflow-x: scroll;
            overflow-y: hidden;
            white-space: nowrap;
            padding: 3px;
        }

        .tugas-card {
            height: 70px;
            width: 100%;
            border-radius: 10px;
            display: inline-block;
            background-repeat: no-repeat;
            background-size: cover;
            background-color: white;
            box-shadow: 0px 1px 4px rgb(210 210 210);
        }

        .tugas-list-icon {
            height: 35px;
            width: 35px;
            /* background-color: white; */
            /* background-image: url('./../icon/fitur/list-icon.svg'); */
            background-position: center;
            background-repeat: no-repeat;
            border-radius: 50%;
            box-shadow: 0px 0px 4px rgba(0, 0, 0, 0.06);
        }
    </style>
@endsection
@section('content')
    <main class="home-app" style="margin-bottom: 10em;">
        <div class="row">
            <div class="col-12 banner-home">
                <h6 class="text-bold text-white mt-5">
                    Hi, Kenya Zahra
                </h6>
                <p class="text-white" style="font-weight: lighter">
                    Selamat datang di SMK Muhammadiyah 1 Weleri. Selamat belajar.....
                </p>
            </div>
            <!-- <hr> -->
            <div class="col-12 mt-4">
                <p class="judul-section">Fitur buat kamu</p>
                <p class="text-muted">Fitur menarik mananti kamu nih</p>
            </div>
            <div class="col-12 mt-2">
                <div class="row text-center">
                    <div class="col-3">
                        <a href="./../tugas/tugas.html">
                            <div class="row fitur-frame d-flex justify-content-center">
                                <div class="fitur" style="background-image: url('./../icon/fitur/list-icon.svg');">
                                </div>
                            </div>
                            <p class="text-muted" style="padding-top: 6px;">Tugas</p>
                        </a>
                    </div>
                    <div class="col-3">
                        <a href="./../pengumuman/pengumuman-list.html">
                            <div class="row fitur-frame d-flex justify-content-center">
                                <div class="fitur"
                                    style="background-image: url('./../icon/fitur/pengumuman-icon.svg');"></div>
                            </div>
                            <p class="text-muted" style="padding-top: 6px;">Pengumuman</p>
                        </a>
                    </div>
                    <div class="col-3">
                        <a href="./../modul/modul.html">
                            <div class="row fitur-frame d-flex justify-content-center">
                                <div class="fitur" style="background-image: url('./../icon/fitur/modul-icon.svg');">
                                </div>
                            </div>
                            <p class="text-muted" style="padding-top: 6px;">Modul</p>
                        </a>
                    </div>
                    <div class="col-3">
                        <div class="row fitur-frame d-flex justify-content-center">
                            <div class="fitur" style="background-image: url('./../icon/fitur/lainnya-icon.svg');">
                            </div>
                        </div>
                        <p class="text-muted" style="padding-top: 6px;">Lainnya</p>
                    </div>
                </div>
            </div>
            <div class="col-12 mt-4">
                <!-- <hr> -->
                <p class="judul-section">Jangan sampai kelewat....!!</p>
                <p class="text-muted">Ayok jangan sampai kamu lewatin nih nanti rugi loh</p>
                <div class="pengumuman-card mt-3"
                    style="background-image: url('./../icon/banner/banner-home.svg');"></div>
            </div>
            <div class="col-12 mt-4">
                <!-- <hr> -->
                <p class="judul-section">Ada apa di MUHI?</p>
                <p class="text-muted">Cara tahu ada up date apa di MUHI jangan sampai kamu ketinggalan !!!</p>
                <div class="pengumuman-list-x mt-3">
                    <div class="col-10 pengumuman-card"
                        style="background-image: url('./../icon/banner/event-1.png');">
                    </div>
                    <div class="col-10 pengumuman-card"
                        style="background-image: url('./../icon/banner/banner-home.svg');">
                    </div>
                    <div class="col-10 pengumuman-card"
                        style="background-image: url('./../icon/banner/banner-home.svg');">
                    </div>
                </div>
            </div>
            <div class="col-12 mt-4">
                <!-- <hr> -->
                <p class="judul-section">Buruan nanti telat</p>
                <p class="text-muted">Sepertinya kamu lupa mengerjakan tugas kamu nih, yuk buruan kerjaiin mumpung
                    masih ada
                    waktu..... </p>
                <div class="tugas-list-x mt-3">
                    <div class="col-10 tugas-card">
                        <div class="row mt-3">
                            <div class="col-2 tugas-home-frame">
                                <div class="tugas-home"
                                    style="background-image: url('./../icon/fitur/list-icon.svg');"></div>
                            </div>
                            <div class="col-10">
                                <b>PRE-TEST Bahasa Arab</b>
                                <p class="text-muted">12 Juli</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-10 tugas-card">
                        <div class="row mt-3">
                            <div class="col-2 tugas-home-frame">
                                <div class="tugas-home"
                                    style="background-image: url('./../icon/fitur/list-icon.svg');"></div>
                            </div>
                            <div class="col-10">
                                <b>PRE-TEST Bahasa Arab</b>
                                <p class="text-muted">12 Juli</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-10 tugas-card">
                        <div class="row mt-3">
                            <div class="col-2 tugas-home-frame">
                                <div class="tugas-home"
                                    style="background-image: url('./../icon/fitur/list-icon.svg');"></div>
                            </div>
                            <div class="col-10">
                                <b>PRE-TEST Bahasa Arab</b>
                                <p class="text-muted">12 Juli</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 mt-5 mb-3 text-center">
                <img src="./../icon/fitur/box_empty.svg" class="mt-2" alt="">
                <p class="px-5 mt-3">Owh tidak, tidak ada informasi lagi buat kamu</p>
            </div>
        </div>
    </main>
@endsection
@section('script')
    
@endsection