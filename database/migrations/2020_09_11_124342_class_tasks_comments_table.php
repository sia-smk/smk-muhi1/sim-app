<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ClassTasksCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('class_tasks_comments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('class_task_id')->unsigned();
            $table->bigInteger('user_id')->unsigned();     
            $table->string('comments');

            $table->timestamps();
            $table->foreign('class_task_id')->references('id')->on('class_tasks');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('class_tasks_comments');
    }
}
