<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class OtpRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('otp_request', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('otp');
            $table->enum('status', ['ACTIVE', 'INACTIVE']);
            $table->date('expired_date');
            $table->string('type', 'login', 'register', 'change_password');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
