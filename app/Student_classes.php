<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student_classes extends Model
{
    public function classes()
    {
        return $this->hasOne('App/Classes');
    }

    public function student()
    {
        return $this->hasOne('App/Student');
    }
}
