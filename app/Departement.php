<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Departement extends Model
{
    public function classes()
    {
        return $this->belongsTo('App/Classes');
    }

    public function subject()
    {
        return $this->hasMany('App/Departement');
    }
}
