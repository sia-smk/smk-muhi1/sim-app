<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Announcement_attachment extends Model
{
    public function announcement()
    {
        return $this->belongsTo('App/Announcement');
    }
}
