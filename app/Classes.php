<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Classes extends Model
{
    public function departement()
    {
        return $this->hasOne('App/Departement');
    }

    public function class_tasks()
    {
        return $this->hasMany('App/Class_tasks');
    }
}
