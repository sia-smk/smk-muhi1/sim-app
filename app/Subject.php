<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    public function teacher()
    {
        return $this->belongsTo('App/Teacher');
    }

    public function departement()
    {
        return $this->belongsTo('App/Departement');
    }

    public function subject_schedulle()
    {
        return $this->hasMany('App/Subject_schedulle');
    }

    public function classes()
    {
        return $this->hasMany('App/Classes');
    }

    public function class_tasks()
    {
        return $this->hasMany('App/Class_tasks');
    }
}
