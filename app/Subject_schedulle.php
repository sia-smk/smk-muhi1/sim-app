<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject_schedulle extends Model
{
    public function subject()
    {
        return $this->belongsto('App/Subject');
    }

    public function classes()
    {
        return $this->belongsTo('App/Classes');
    }

    public function Attendance()
    {
        return  $this->hasOne('App/Attendance');
    }
}
