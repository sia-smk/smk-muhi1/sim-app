<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Class_tasks_comment extends Model
{
    public function class_tasks()
    {
        return $this->belongsTo('App/Class_tasks');
    }

    public function user()
    {
        return $this->belongsTo('App/User');
    }
}
