<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    public function user()
    {
        return $this->belongsTo('App/User');
    }

    public function subject_schedulle()
    {
        return $this->belongsTo('App/Subject_schedulle');
    }
}
