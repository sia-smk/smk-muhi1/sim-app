<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    public function users()
    {
        return $this->belongsTo('App/User');
    }

    public function student_classes()
    {
        return $this->belongsTo('App/Student_classes');
    }

}
