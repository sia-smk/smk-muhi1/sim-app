<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Class_tasks extends Model
{
    public function classes()
    {
        return $this->belongsTo('App/Classes');
    }

    public function subject()
    {
        return $this->belongsTo('App/Subject');
    }

    public function class_tasks_comment()
    {
        return $this->hasMany('class_tasks_comment');
    }
}
